<?php

require 'CsvApp/CsvFormatter.php';
require 'CsvApp/CsvReader.php';
require 'View/viewGenerator.php';

    $path = 'list.csv';
    $file = new \csvApp\csvReader($path);
    $csvData = $file->csvDecode();
    $formatter = new \csvApp\csvFormatter($csvData);
    $formattedCsv = $formatter->csvFormatter();
    $view = new \View\ViewGenerator($formattedCsv, basename($path,".csv"));
    $view->render();
